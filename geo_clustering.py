import numpy as np
import pandas as pd
import statsmodels.api as sm
from typing import Tuple, List, Union
from numpy import ndarray
from pandas import DataFrame
from shapely.geometry import Point, MultiPoint, Polygon, MultiPolygon
from sklearn.cluster import KMeans, DBSCAN
from sklearn.preprocessing import StandardScaler


def geo_clusters_on_unexplained_var_wrapper(
    data: DataFrame,
    target_variable: str,
    lat_0: str,
    lon_0: str,
    lat_1: str = None,
    lon_1: str = None,
    covariates: List[str] = [],
    geo_cluster_weight: float = 0.5,
    raise_na_error: bool = True,
    cluster_column: str = "cluster_id",
    cluster_wkt_column: str = "cluster_wkt",
    n_clusters: int = 10,
    init: str = "k-means++",
    n_init: int = 5,
    random_state: int = None,
    eps: float = 0.2,
    min_samples: int = 10,
) -> Tuple[DataFrame, DataFrame]:
    """
    Builds clusters based on the unexplained variation of target_variable
    after controling for columns in covariates. It also infered the wkt
    representation of the shape of those clusters.

    This is a memory heavy function based on Pandas, but its main code
    is done in func:geo_clusters_on_unexplained_var, which is based on
    numpy and shapely.

    Parameters
    ----------
    data: Pandas Df
        input data must contain columns:
            target_variable, covariates, lat_0, lon_0, and if given lat_1 and lon_1.
    target_variable: str
        name of outcome variable based on the unexplained variation of which the
        clusters are made.
    lat_0: str
        name of column that stores the (initial) latitude of the (arc) point.
    lon_0: str
        name of column that stores the (initial) longitude of the (arc) point.
    lat_1: str
        if given, name of column that stores the final latitude of the arc.
    lon_1: str
        if given, name of column that stores the final longitude of the arc.
    covariates: List[str]
        list of features that are controlled for.
    geo_cluster_weight: float
        value between zero and one. A zero value will result on the clusters being
        based only on the unexplained variation. A one value will results on the
        clusters being based only on the geo variation.
    raise_na_error: bool
        True will raises error if Null values are detected in relevant columns
        of data. False will drop those rows and continue.
    cluster_column: str
        name of created column that will store the ids of the created clusters.
    cluster_wkt_column: str
        name of created column that will store the wkt of the created clusters.

    Parameters KMeans
    -----------------
    n_clusters: int
        positive integer that stores the number of clusters to be created.
    init: "k-means++" or "random"
        See docs: https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
    n_init: int
        See docs: https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
    random_state: int
        controls initialization of clustering. Use same number across multiply
        runs to get deterministic results

    Parameters DBSCAN
    ------------------
    eps : float, default=0.2
        The maximum distance between two samples for one to be considered
        as in the neighborhood of the other. This is not a maximum bound
        on the distances of points within a cluster. This is the most
        important DBSCAN parameter to choose appropriately for your data set
        and distance function.
    min_samples : int, default=5
        The number of samples (or total weight) in a neighborhood for a point
        to be considered as a core point. This includes the point itself.

    Output
    ------
    data: Pandas Df
        copy of data with additional column cluster_column. This column contains
        the cluster labels assign to each point. The labels of the assigned points
        are integers between 1 and k, where k is the final number of clusters.
        In addition, -1 is used for points not assigned to a cluster (noise).
    clusters_wkt: Pandas Df
        Small DataFrame with columns cluster_column and cluster_wkt_column.
        The former stores the unique cluster labels and the later the wkt
        representation of its infered shape.
    """
    N_CLUSTERS_LIMIT = 20
    RESID_TEMP_COL = "resid__temp__col"
    ##############################
    # A. Validate input parameters
    ##############################
    # A1. Simple validations
    try:
        assert isinstance(data, DataFrame)
    except AssertionError:
        raise AssertionError("Input data must be a Pandas DataFrame.")
    try:
        assert isinstance(target_variable, str)
        assert target_variable in data.columns
        assert pd.api.types.is_numeric_dtype(data[target_variable])
    except AssertionError:
        raise AssertionError("Input target_variable must be a string representing" " a numeric column of input data.")
    try:
        assert isinstance(covariates, list)
        for i in covariates:
            assert isinstance(i, str)
            assert i in data.columns
            assert pd.api.types.is_numeric_dtype(data[i])
    except AssertionError:
        raise AssertionError(
            "Input covariates must be a list of strings. Each of those"
            " must represent a numeric column of input data."
        )
    try:
        assert isinstance(geo_cluster_weight, (int, float))
        assert (geo_cluster_weight >= 0) and (geo_cluster_weight <= 1)
    except AssertionError:
        raise AssertionError(
            "Input geo_cluster_weight must be a float or integer" " between zero (inclusive) and one (inclusive)."
        )
    for i in [lat_0, lon_0]:
        try:
            assert isinstance(i, str)
            assert i in data.columns
            assert pd.api.types.is_numeric_dtype(data[i])
        except AssertionError:
            raise AssertionError(
                "Input lat_0 and lon_0 must be strings representing" " numeric columns of input data."
            )
    if (lat_1 is not None) or (lon_1 is not None):
        for i in [lat_1, lon_1]:
            try:
                assert isinstance(i, str)
                assert i in data.columns
                assert pd.api.types.is_numeric_dtype(data[i])
            except AssertionError:
                raise AssertionError(
                    "If either of inputs lat_1 and lon_1 is given, then both must be"
                    " strings representing numeric columns of input data."
                )
    try:
        assert isinstance(raise_na_error, bool)
    except AssertionError:
        raise AssertionError("Input raise_na_error must be boolean.")
    try:
        assert isinstance(cluster_column, str)
        assert cluster_column not in data.columns
    except AssertionError:
        raise AssertionError(
            "Input cluster_column must be a string that is not already used" " in the columns of input data."
        )
    try:
        assert isinstance(n_clusters, int)
        assert (n_clusters > 0) and (n_clusters <= N_CLUSTERS_LIMIT)
    except AssertionError:
        raise AssertionError(
            "Input n_clusters must be positive integer lower than," f" or equal to, {N_CLUSTERS_LIMIT}."
        )
    try:
        assert isinstance(init, str) and (init in ("k-means++", "random"))
    except AssertionError:
        raise AssertionError("Input init must be either 'k-means++' or 'random'.")
    try:
        assert isinstance(n_init, int) and (n_init > 0)
    except AssertionError:
        raise AssertionError("Input n_init must be positive integer.")
    if random_state is not None:
        try:
            assert isinstance(random_state, int)
        except AssertionError:
            raise AssertionError("Input random_state must be integer.")
    try:
        assert isinstance(eps, (float, int)) and eps >= 0
    except AssertionError:
        raise AssertionError("Input eps must be non-negative float or integer.")
    try:
        assert isinstance(min_samples, int) and min_samples > 0
    except AssertionError:
        raise AssertionError("Input min_samples must be positive integer.")
    # A2. Validation or update of rows of data
    numb_cols = [target_variable, *covariates]
    if (lat_1 is None) or (lon_1 is None):
        geom_cols = [lat_0, lon_0]
    else:
        geom_cols = [lat_0, lon_0, lat_1, lon_1]
    relv_cols = [*numb_cols, *geom_cols]
    data = data[relv_cols]
    if raise_na_error:
        try:
            assert len(data) == len(data.dropna())
        except AssertionError:
            raise AssertionError("Rows with Nulls detected in relevant columns of input data.")
    else:
        data = data.dropna()
    ##################
    # B. Make clusters
    ##################
    labels, clusters_wkt_list = geo_clusters_on_unexplained_var(
        target_variable=data[target_variable].values,
        lat_0=data[lat_0].values,
        lon_0=data[lon_0].values,
        lat_1=None if lat_1 is None else data[lat_1].values,
        lon_1=None if lon_1 is None else data[lon_1].values,
        covariates=None if len(covariates) == 0 else data[covariates].values,
        geo_cluster_weight=geo_cluster_weight,
        n_clusters=n_clusters,
        init=init,
        n_init=n_init,
        random_state=random_state,
        eps=eps,
        min_samples=min_samples,
    )
    data.loc[:, cluster_column] = labels
    cluster_wkt = {cluster_column: [], cluster_wkt_column: []}
    for i in clusters_wkt_list:
        cluster_wkt[cluster_column].append(i[0])
        cluster_wkt[cluster_wkt_column].append(i[1])
    ############################
    # B. Gather data on clusters
    ############################
    data_agg = data.groupby(by=cluster_column, as_index=False).agg({i: "mean" for i in [target_variable, *covariates]})
    cluster_wkt = DataFrame(cluster_wkt).merge(
        data_agg,
        on=cluster_column,
        how="left",
    )[[cluster_column, target_variable, *covariates, cluster_wkt_column]]
    return data, cluster_wkt


def geo_clusters_on_unexplained_var(
    target_variable: ndarray,
    lat_0: ndarray,
    lon_0: ndarray,
    lat_1: ndarray = None,
    lon_1: ndarray = None,
    covariates: ndarray = None,
    geo_cluster_weight: float = 0.5,
    n_clusters: int = 10,
    init: str = "k-means++",
    n_init: int = 5,
    random_state: int = None,
    eps: float = 0.2,
    min_samples: int = 10,
) -> Tuple[ndarray, List[List[Union[int, str]]]]:
    """
    Builds clusters based on the unexplained variation of target_variable
    after controling for columns in covariates. It also infered the wkt
    representation of the shape of those clusters.

    This is a memory light function based on numpy, but
        1. it does not check for Nulls in inputs
        2. it does not do any other checks on the type of inputs

    Parameters
    ----------
    target_variable: numpy ndarray
        single dimesion array containing the values of the variable based on
        which the unexplained variation of which the clusters are made.
    lat_0: numpy ndarray
        single dimesion array containing the (initial) latitude of the (arc) point.
    lon_0: numpy ndarray
        single dimesion array containing the (initial) longitude of the (arc) point.
    lat_1: numpy ndarray
        single dimesion array containing the final latitude of the arc.
    lon_1: numpy ndarray
        single dimesion array containing the final longitude of the arc.
    covariates: numpy ndarray
        array containing the values of the covariates that we control for.
    geo_cluster_weight: float
        value between zero and one. A zero value will result on the clusters being
        based only on the unexplained variation. A one value will results on the
        clusters being based only on the geo variation.

    Parameters KMeans
    -----------------
    n_clusters: int
        positive integer that stores the number of clusters to be created.
    init: "k-means++" or "random"
        See docs: https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
    n_init: int
        See docs: https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
    random_state: int
        controls initialization of clustering. Use same number across multiply
        runs to get deterministic results

    Parameters DBSCAN
    ------------------
    eps : float, default=0.2
        The maximum distance between two samples for one to be considered
        as in the neighborhood of the other. This is not a maximum bound
        on the distances of points within a cluster. This is the most
        important DBSCAN parameter to choose appropriately for your data set
        and distance function.
    min_samples : int, default=5
        The number of samples (or total weight) in a neighborhood for a point
        to be considered as a core point. This includes the point itself.

    Ouputs
    ------
    clusters_labels: ndarray
        single dimesion array containing the cluster labels assign to each point.
        The labels of the assigned points are integers between 1 and k, where k
        is the final number of clusters. In addition, -1 is used for points not
        assigned to a cluster (noise).
    clusters_wkt: List[List[Union[int, str]]]
        list of which each row has the id of the cluster and the wkt representation
        of its infered shape.
    """
    # 1. Get unexplained variation of target_variable
    if covariates is None:
        resid = target_variable
    else:
        res = sm.OLS(target_variable, sm.add_constant(covariates)).fit(cov_type="HC1")
        resid = res.resid
    # 2. Stuck and scale data
    if (lon_1 is None) or (lat_1 is None):
        x = np.column_stack((resid, lon_0, lat_0))
    else:
        x = np.column_stack((resid, lon_0, lat_0, lon_1, lat_1))
    x = StandardScaler().fit(x).transform(x)
    # 3. Adjust relative importance of resid vs geo data
    w = geo_cluster_weight
    if (lon_1 is None) or (lat_1 is None):
        x = x * np.array([1 - geo_cluster_weight, 0.5 * geo_cluster_weight, 0.5 * geo_cluster_weight])
    else:
        x = x * np.array(
            [
                1 - geo_cluster_weight,
                0.25 * geo_cluster_weight,
                0.25 * geo_cluster_weight,
                0.25 * geo_cluster_weight,
                0.25 * geo_cluster_weight,
            ]
        )
    # 4. Initial Clustering
    labels = KMeans(n_clusters=n_clusters, init=init, n_init=n_init, random_state=random_state).fit(x).labels_
    labels_unique = np.sort(np.unique(labels))
    # 5. Get shapes of each cluster
    points = np.column_stack((lon_0, lat_0))
    cluster_shapes = []
    for label in labels_unique:
        cluster_shapes.append(get_points_shape(points[labels == label, :], eps=eps, min_samples=min_samples))
    # 6. Get mutually exclusive shapes
    mutually_exclusive_shapes = get_mutually_exclusive_shapes(cluster_shapes)
    # 7. Allocate points to mutually exclusive shapes
    nr_shapes = len(mutually_exclusive_shapes)
    clusters_labels = []
    for point in points:
        i = 0
        while (i < nr_shapes) and (not mutually_exclusive_shapes[i].contains(Point(point))):
            i += 1
        clusters_labels.append(i + 1 if i < nr_shapes else -1)
    # 8. Prepape shape output
    clusters_wkt = [[i + 1, shape.wkt] for i, shape in enumerate(mutually_exclusive_shapes)]
    return np.array(clusters_labels), clusters_wkt


def get_points_shape(points: ndarray, eps: float = 0.2, min_samples: int = 10) -> Union[Polygon, MultiPolygon]:
    """
    Infer approximate shape of points by applying DBSCAN and finding the union
    of the convex hulls of the derived clusters.

    Parameters
    ----------
    points: ndarray
        Numpy array of shape (n, 2), where n is the number of points considered

    Parameters DBSCAN
    ------------------
    eps : float, default=0.2
        The maximum distance between two samples for one to be considered
        as in the neighborhood of the other. This is not a maximum bound
        on the distances of points within a cluster. This is the most
        important DBSCAN parameter to choose appropriately for your data set
        and distance function.
    min_samples : int, default=5
        The number of samples (or total weight) in a neighborhood for a point
        to be considered as a core point. This includes the point itself.

    Output
    ------
    Shapely Multipolygon, or polygon, with the infered shape of the cluster
    """
    points_scaled = StandardScaler().fit(points).transform(points)
    labels = DBSCAN(eps=eps, min_samples=min_samples).fit(points_scaled).labels_
    unique_labels = np.sort(np.unique(labels))
    if unique_labels[0] == -1:
        unique_labels = unique_labels[1:]
    if len(unique_labels) == 0:
        return MultiPoint(points).convex_hull
    shape = MultiPoint(points[labels == unique_labels[0], :]).convex_hull
    for label in unique_labels[1:]:
        shape = shape.union(MultiPoint(points[labels == label, :]).convex_hull)
    return shape


def get_mutually_exclusive_shapes(shapes: List[Union[Polygon, MultiPolygon]]) -> List[Polygon]:
    """
    Flatten polygons and multipolygons in shapes and break then in mutually exclusive shapes.

    Parameter
    ---------
    shapes: List of Polygons and Multipolygons

    Output
    ------
    List of mutually exclusive polygons, of which the union is that same with that
    of the geometries in shapes
    """
    # Get flatten shapes
    shapes_flatten, areas_flatten = [], []
    for shape in shapes:
        if shape.geometryType() == "Polygon":
            shapes_flatten.append(shape)
            areas_flatten.append(shapes_flatten[-1].area)
        elif shape.geometryType() == "MultiPolygon":
            for geom in shape.geoms:
                shapes_flatten.append(geom)
                areas_flatten.append(shapes_flatten[-1].area)
    # Short flatten shapes by area size
    shapes_flatten = [shapes_flatten[i] for i in np.argsort(areas_flatten)]
    del areas_flatten
    # Find mutually exclusive shapes
    shapes_union = shapes_flatten[0]
    mutually_exclusive_shapes = [shapes_flatten[0]]
    for shape in shapes_flatten[1:]:
        shape_difference = shape.difference(shapes_union)
        if shape_difference.geometryType() == "Polygon":
            mutually_exclusive_shapes.append(shape_difference)
        elif geom.geometryType() == "MultiPolygon":
            for geom in shape_difference.geoms:
                mutually_exclusive_shapes.append(geom)
        shapes_union = shapes_union.union(shape_difference)
    # Sort by size before returning
    areas = [i.area for i in mutually_exclusive_shapes]
    mutually_exclusive_shapes = [mutually_exclusive_shapes[i] for i in np.argsort(areas)]
    del areas
    return mutually_exclusive_shapes
